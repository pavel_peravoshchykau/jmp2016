package com.epam.mentoring.nosql.mongodb.dao;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import org.bson.types.ObjectId;

import static com.mongodb.client.model.Filters.eq;

public class MondoObjDao {

    private MongoClient mongoClient;
    private MongoDatabase mongoDatabase;
    MongoCollection<Document> collection;

    public MondoObjDao(String host, int port, String dbName, String collectionName) {
        this.mongoClient = new MongoClient(host, port);
        this.mongoDatabase = mongoClient.getDatabase(dbName);
        this.collection = mongoDatabase.getCollection(collectionName);
    }

    public void addDocument(Document doc) {
        collection.insertOne(doc);
    }

    public FindIterable<Document> findDocument(String fieldName, String searchValue) {
        return collection.find(eq(fieldName, searchValue));
    }

    public void deleteDocument(String fieldName, String searchValue) {
        collection.deleteOne(eq(fieldName, searchValue));
    }

    public DeleteResult deleteDocument(String id) {
        return collection.deleteOne(eq("_id", new ObjectId(id)));
    }
}
