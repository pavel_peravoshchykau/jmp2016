package com.epam.mentoring.jpa.hibernate.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "project")
public class Project implements Serializable {
    private int id;
    private Set<Employee> employees = new HashSet<>();

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "PROJECT_ID", unique = true, nullable = false)
    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "projects")
    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }
}
