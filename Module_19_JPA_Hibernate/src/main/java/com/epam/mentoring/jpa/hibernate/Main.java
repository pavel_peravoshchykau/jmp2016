package com.epam.mentoring.jpa.hibernate;

import com.epam.mentoring.jpa.hibernate.models.*;
import com.epam.mentoring.jpa.hibernate.service.EmployeeService;
import com.epam.mentoring.jpa.hibernate.service.ProjectService;
import com.epam.mentoring.jpa.hibernate.service.UnitService;

public class Main {

    public static void main(String[] args) {

        Unit unit = new Unit();
        unit.setId(1);

        Employee employee = new Employee();
        employee.setFirstName("Pavel");
        employee.setLastName("Peravoshchykau");
        employee.setEmployeeStatus(EmployeeStatus.BILLABLE);

        //unit.addEmployee(employee);

        employee.setUnit(unit);

        Personal personal = new Personal();
        personal.setPersonalInfo("very personal information");
        employee.setPersonal(personal);
        personal.setEmployee(employee);

        Project project = new Project();
        project.setId(1);

        ProjectService projectService = new ProjectService();
        UnitService unitService = new UnitService();
        EmployeeService employeeService = new EmployeeService();

        projectService.saveProject(project);
        unitService.saveUnit(unit);
        employeeService.saveEmployee(employee);

        projectService.addEmployeeToProject(project.getId(), employee);
        unitService.addEmployeeToUnit(unit.getId(), employee);
        employeeService.deleteEmployee(employee.getId());

        System.out.println("Done");
    }
}
