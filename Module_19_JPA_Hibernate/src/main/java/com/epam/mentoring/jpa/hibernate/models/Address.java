package com.epam.mentoring.jpa.hibernate.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {

    String street;
    String city;

    @Column(name = "STREET")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Column(name = "CITY")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
