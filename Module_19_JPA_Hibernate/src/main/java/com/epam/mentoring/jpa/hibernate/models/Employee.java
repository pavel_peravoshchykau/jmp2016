package com.epam.mentoring.jpa.hibernate.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {
    private int id;
    private String firstName;
    private String lastName;
    private Personal personal;
    private Address address;
    private EmployeeStatus employeeStatus;
    private Set<Project> projects = new HashSet<>();
    private Unit unit;

    public Employee() {
    }

    public Employee(int id, String firstName, String lastName, Personal personal, Address address) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.personal = personal;
        this.address = address;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "FIRST_NAME")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "LAST_NAME")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="CITY", column=@Column(table="employee")),
            @AttributeOverride(name="STREET", column=@Column(table="employee"))
    })
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "employee", cascade = CascadeType.ALL)
    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    @Enumerated(EnumType.STRING)
    public EmployeeStatus getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(EmployeeStatus employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "employees_project", joinColumns = {
            @JoinColumn(name = "ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "PROJECT_ID",
                    nullable = false, updatable = false) })
    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    //@JoinColumn(name = "UNIT_ID", nullable = false)
    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
