package com.epam.mentoring.jpa.hibernate.models;

public enum EmployeeStatus {
    BILLABLE,
    TRAINEE,
    ON_BENCH,
    FARMCLUB
}
