package com.epam.mentoring.jpa.hibernate.service;

import com.epam.mentoring.jpa.hibernate.models.Employee;
import org.hibernate.Query;

import java.util.List;

public class EmployeeService extends CommonService {

    public void saveEmployee(Employee employee) {
        saveObject(employee);
    }

    public Employee findEmployee(int id) {
        String hql = "FROM Employee WHERE id = :id";
        Query query = session.createQuery(hql);
        query.setParameter("id", id);
        List results = query.list();
        return results.isEmpty() ? null : (Employee) results.get(0);
    }

    public void deleteEmployee(int id) {
        session.beginTransaction();
        session.delete(findEmployee(id));
        session.getTransaction().commit();
    }

    public void booleanUpdateEmployee(Employee employee) {
        session.beginTransaction();
        session.saveOrUpdate(employee);
        session.getTransaction().commit();
    }
}
