package com.epam.mentoring.jpa.hibernate.service;

import com.epam.mentoring.jpa.hibernate.util.HibernateUtil;
import org.hibernate.Session;

public abstract class CommonService {
    protected Session session = HibernateUtil.getSessionFactory().openSession();


    protected void saveObject(Object object) {
        session.beginTransaction();
        session.save(object);
        session.getTransaction().commit();
    }

}

