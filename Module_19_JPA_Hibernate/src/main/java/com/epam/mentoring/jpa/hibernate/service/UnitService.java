package com.epam.mentoring.jpa.hibernate.service;

import com.epam.mentoring.jpa.hibernate.models.Employee;
import com.epam.mentoring.jpa.hibernate.models.Unit;
import org.hibernate.Query;

import java.util.List;

public class UnitService extends CommonService {

    public void saveUnit(Unit unit) {
        saveObject(unit);
    }

    public Unit findUnit(int id) {
        String hql = "FROM Unit WHERE UNIT_ID = :UNIT_ID";
        Query query = session.createQuery(hql);
        query.setParameter("UNIT_ID", id);
        List results = query.list();
        return results.isEmpty() ? null : (Unit) results.get(0);
    }

    public void deleteUnit(int id) {
        session.beginTransaction();
        session.delete(findUnit(id));
        session.getTransaction().commit();
    }

    public void updateUnit(Unit unit) {
        session.beginTransaction();
        session.saveOrUpdate(unit);
        session.getTransaction().commit();
    }

    public void addEmployeeToUnit(int id, Employee employee) {
        Unit unit = findUnit(id);
        unit.addEmployee(employee);
        updateUnit(unit);
    }
}
