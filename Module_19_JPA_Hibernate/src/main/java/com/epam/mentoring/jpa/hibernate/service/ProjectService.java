package com.epam.mentoring.jpa.hibernate.service;

import com.epam.mentoring.jpa.hibernate.models.Employee;
import com.epam.mentoring.jpa.hibernate.models.Project;
import org.hibernate.Query;

import java.util.List;

public class ProjectService extends CommonService {

    public void saveProject(Project project) {
        saveObject(project);
    }

    public Project findProject(int id) {
        String hql = "FROM Project WHERE PROJECT_ID = :PROJECT_ID";
        Query query = session.createQuery(hql);
        query.setParameter("PROJECT_ID", id);
        List results = query.list();
        return results.isEmpty() ? null : (Project) results.get(0);
    }

    public void deleteProject(int id) {
        session.beginTransaction();
        session.delete(findProject(id));
        session.getTransaction().commit();
    }

    public void updateProject(Project project) {
        session.beginTransaction();
        session.saveOrUpdate(project);
        session.getTransaction().commit();
    }

    public void addEmployeeToProject(int id, Employee employee) {
        Project project = findProject(id);
        project.addEmployee(employee);
        updateProject(project);
    }

}
