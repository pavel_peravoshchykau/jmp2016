package com.company;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Main {

    private static void printSystemInfo(){
        Runtime runtime = Runtime.getRuntime();

        NumberFormat format = NumberFormat.getInstance();

        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();

        System.out.println("free memory: " + format.format(freeMemory / 1024));
        System.out.println("allocated memory: " + format.format(allocatedMemory / 1024));
        System.out.println("max memory: " + format.format(maxMemory / 1024));
        System.out.println("total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024));
        System.out.println("-------------------------------------------------------------------------------------------------------");
    }

//    private void testReferences() {
//        ReferenceQueue referenceQueue = new ReferenceQueue();
//
//        MyDouble pi = new MyDouble(3.14, "pi");
//        MyDouble g = new MyDouble(9.8, "g");
//        MyDouble e = new MyDouble(2.78, "e");
//
//        WeakReference<MyDouble> weakPi = new WeakReference<MyDouble>(pi, referenceQueue);
//        SoftReference<MyDouble> softG = new SoftReference<MyDouble>(g, referenceQueue);
//        PhantomReference<MyDouble> phantomE = new PhantomReference<>(e, referenceQueue);
//
//        System.out.println("Instrong times");
//        System.out.println(weakPi.get());
//        System.out.println(softG.get());
//        System.out.println(phantomE.get());
//
//        pi = null;
//        g = null;
//        e = null;
//
//
//        System.out.println("After null");
//        System.out.println(weakPi.get());
//        System.out.println(softG.get());
//        System.out.println(phantomE.get());
//
//        System.gc();
//
//        System.out.println("After GC");
//        System.out.println(weakPi.get());
//        System.out.println(softG.get());
//        System.out.println(phantomE.get());
//
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e1) {
//            e1.printStackTrace();
//        }
//
//        System.out.println("RQ" + referenceQueue.poll());
//        System.out.println("RQ" + referenceQueue.poll());
//        System.out.println("RQ" + referenceQueue.poll());
//    }

    private static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        int cycles = 100000;
        int maxSize = 100000;
        int shutdownCounter = 5;
        int start = Calendar.getInstance().get(Calendar.MINUTE);
        List<Long> longs = new ArrayList<>();
        sleep(1000);
        while(true) {
            long random = Math.round(Math.random()*cycles);
            for (long i = 0; i < random; i++) {
                longs.add(Math.round(Math.random()*cycles));
                if (Math.round(Math.random()*10)%2 == 0) {
                    int toRemove = (int) Math.round(Math.random()*longs.size());
                    if (toRemove == longs.size()) {
                        toRemove -= 1;
                    }
                    longs.remove(toRemove);
                }
                if (longs.size() > maxSize) {
                    longs.subList(0, maxSize/2).clear();
                }
            }
            System.out.println(longs.size());
            int currentMinute = Calendar.getInstance().get(Calendar.MINUTE);
            if (currentMinute != start){
                start = currentMinute;
                shutdownCounter -=1;
            }
            if (shutdownCounter <= 0) {
                System.out.println("Exit by timer");
                System.exit(0);
            }
        }
    }
}


// class MyDouble {
//     private Double value;
//     private String name;
//
//     public MyDouble(Double value, String name) {
//         this.value = value;
//         this.name = name;
//     }
//
//     @Override
//     protected void finalize() {
//         System.out.println("the " + name + " with " + value + " will be finalized");
//     }
//
//     public String toString() {
//         return this.name + " " + this.value;
//     }
// }