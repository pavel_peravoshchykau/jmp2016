package com.company.classloader;

import java.io.*;

public class ClassPathClassLoader extends ClassLoader {

    private final String classPath;
    private final String fsSeparator = File.separator;

    public ClassPathClassLoader(String classPath) {
        super(ClassPathClassLoader.class.getClassLoader());
        String localPath = classPath;
        if (!(classPath.charAt(classPath.length() - 1) == fsSeparator.charAt(0))) {
            localPath = classPath.concat(fsSeparator);
        }
        this.classPath = localPath;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return super.loadClass(name);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        try {
            String path = classPath.concat(name.replace(".", fsSeparator).concat(".class"));
            byte b[] = fetchClassFromFS(path);
            return defineClass(name, b, 0, b.length);
        } catch (FileNotFoundException ex) {
            return super.findClass(name);
        } catch (IOException ex) {
            return super.findClass(name);
        }
    }

    /**
     * Reading a File into a Byte Array
     * @param path path to the file
     * @return file byte array
     * @throws IOException
     */
    private byte[] fetchClassFromFS(String path) throws IOException {
        InputStream is = new FileInputStream(new File(path));
        // Get the size of the file
        long length = new File(path).length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        // Create the byte array to hold the data
        byte[] bytes = new byte[(int)length];
        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }
        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+path);
        }
        // Close the input stream and return bytes
        is.close();
        return bytes;
    }
}
