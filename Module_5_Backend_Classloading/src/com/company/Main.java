package com.company;

import com.company.classloader.ClassPathClassLoader;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;

public class Main {

    public static void main(String[] args) {

        ClassPathClassLoader loader;
        Class loadedClass;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input;

        try {
            System.out.println("enter classPath and class name divided by space:");
            while((input = br.readLine()) != null) {
                if (input.isEmpty()) {
                    System.out.println("input is empty");
                } else {
                    String[] arguments = input.split(" ");
                    if (arguments.length == 2 && new File(arguments[0]).exists()) {
                        loader = new ClassPathClassLoader(arguments[0]);
                        try {
                            loadedClass = loader.loadClass(arguments[1]);
                            Object loadedObject = loadedClass.getConstructor().newInstance();
                            loadedClass.getMethod("lever").invoke(loadedObject);

                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            System.err.println("ClassNotFoundException, please try again");
                        }
                    } else {
                        System.out.println("incorrect number of arguments, try something like: C:/mockPath com.epam.mentoring.lessone.Semaphore");
                    }
                }
                System.out.println("enter classPath and class name divided by space:");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
