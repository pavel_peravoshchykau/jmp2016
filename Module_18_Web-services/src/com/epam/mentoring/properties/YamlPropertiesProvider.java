package com.epam.mentoring.properties;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;

import java.util.Properties;

public class YamlPropertiesProvider implements PropertiesProvider {

    final String CONFIG_LOCATION = "WEB-INF/applicationConfig.yml";
    final Properties properties;

    public YamlPropertiesProvider() {
        YamlPropertiesFactoryBean yamlFactoryBean = new YamlPropertiesFactoryBean();
        yamlFactoryBean.setResources(new ClassPathResource(CONFIG_LOCATION));
        properties = yamlFactoryBean.getObject();
    }

    @Override
    public Object getPropertyValue(String propName) {
        return properties.getProperty(propName);
    }
}
