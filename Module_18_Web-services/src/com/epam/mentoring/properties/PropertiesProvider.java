package com.epam.mentoring.properties;

public interface PropertiesProvider {

    Object getPropertyValue(String propName);

}
