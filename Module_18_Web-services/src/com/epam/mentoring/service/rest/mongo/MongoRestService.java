package com.epam.mentoring.service.rest.mongo;

import com.epam.mentoring.nosql.mongodb.dao.MondoObjDao;
import org.bson.Document;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Path("/mongo")
public class MongoRestService {

    MondoObjDao mondoObjDao = new MondoObjDao("localhost", 27017, "test", "jmp");

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Document insertDocument(JSONObject document) {
        System.out.println("Inserting document: " + document);
        mondoObjDao.addDocument(Document.parse(document.toString()));
        return Document.parse(document.toString());
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteDocument(JSONObject document) throws JSONException {
        System.out.println("Deleting document with id: " + document.getString("id"));
        mondoObjDao.deleteDocument(document.getString("id"));
        return Response.ok(document).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Document getDocument(JSONObject document) throws JSONException {
        List<Document> documents = new ArrayList<>();
        System.out.println("Find document: " + document);
        for (Document document1 : mondoObjDao.findDocument(Document.parse(document.toString()))) {
            documents.add(document1);
        }

        return new Document("documents found", documents);
    }

    @GET
    public String hetHello() {
        return "Hello";
    }
}
