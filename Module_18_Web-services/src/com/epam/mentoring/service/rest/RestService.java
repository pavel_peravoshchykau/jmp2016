package com.epam.mentoring.service.rest;

import com.epam.mentoring.service.rest.mongo.MongoRestService;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class RestService extends Application {

    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(MongoRestService.class);
        return s;
    }

}
