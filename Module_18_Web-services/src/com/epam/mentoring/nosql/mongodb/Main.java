package com.epam.mentoring.nosql.mongodb;

import com.epam.mentoring.nosql.mongodb.dao.MondoObjDao;
import com.epam.mentoring.properties.YamlPropertiesProvider;
import org.bson.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    private static final String nameQuery = "Please enter field name";
    private static final String fieldQuery = "Please enter field value";
    private static final String deleteQuery = "Please enter document id";

    private static String queryFromConsole(String message, BufferedReader br) throws IOException {
        System.out.println(message);
        return br.readLine();
    }

    public static void main(String[] args) throws IOException {
        String host = args.length > 0 ? args[0] : "localhost";
        int port = args.length > 1 ? Integer.valueOf(args[1]) : 27017;
        String dbName = args.length > 2 ? args[2] : "test";
        String collectionName = args.length > 3 ? args[3] : "jmp";
        MondoObjDao objDao = new MondoObjDao(host, port, dbName, collectionName);


        YamlPropertiesProvider properties = new YamlPropertiesProvider();
        MondoObjDao mondoObjDao = new MondoObjDao((String)properties.getPropertyValue("host"),
                Integer.valueOf((String) properties.getPropertyValue("port")),
                (String) properties.getPropertyValue("dbName"),
                (String) properties.getPropertyValue("collectionName"));

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while (true) {
            System.out.println("please enter command (-create, -find, -delete)");
            line = br.readLine();
            if (line.startsWith("-")) {
                switch (line) {
                    case "-create":
                        Document document = new Document(queryFromConsole(nameQuery, br),  queryFromConsole(fieldQuery, br));
                        objDao.addDocument(document);
                        break;
                    case "-find":
                        for (Document doc : objDao.findDocument(queryFromConsole(nameQuery, br), queryFromConsole(fieldQuery, br))) {
                            System.out.println("Document found: " + doc);
                        }
                        break;
                    case "-delete":
                        try {
                            System.out.println(objDao.deleteDocument(queryFromConsole(deleteQuery, br)));
                        } catch (IllegalArgumentException ex) {
                            System.out.println("Wrong document id");
                        }
                        break;
                }
            }
        }
    }
}
