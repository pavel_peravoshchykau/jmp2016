package com.epam.mentoring.trioubleshooting;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MemoryLeak {

    static List<String> list = new ArrayList<String>();

    public static void main(String[] args) throws IOException, InterruptedException {
        String line;
        int defaultLength = 3;
        int linesCounter = 0;

        BufferedReader reader = new BufferedReader(new FileReader("input.txt"));

        while ((line = reader.readLine()) != null) {

            if (line.length() >= defaultLength) {
                list.add(line.substring(0, 2));
                Thread.sleep(10);
            }
            linesCounter++;
        }
        for (String s : list) {
            if (s.length() > Math.random()*3.01) {
                System.out.println(s);
            }
        }
        System.out.println("List size: " + list.size());
        System.out.println("Lines: " + linesCounter);
        Thread.sleep(100000);
    }
}
