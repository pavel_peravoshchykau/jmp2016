package com.epam.mentoring.trioubleshooting;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Bottleneck {

    static Queue<Long> queue = new LinkedList<>();

    public static void main(String[] args) throws InterruptedException {
        int numberOfThreads = 10;

        Lock lock = new ReentrantLock();
        Thread.sleep(3000);

        Thread bottleneck = new Thread(() -> {
            while (true) {
                lock.lock();
                Long prev = 0l;
                for (Long aLong : queue) {
                    prev = aLong.compareTo(prev) > 0 ? prev = aLong : prev;
                }
                lock.unlock();
            }
        });

        bottleneck.setName("bottleneck");
        bottleneck.start();

        for (int i = 0; i < numberOfThreads; i++) {
            new Thread(() -> {
                while (true) {
                    lock.lock();
                    queue.add((long) Math.random()*100);
                    lock.unlock();
                }

            }).start();

            new Thread(() -> {
                while (true) {
                    lock.lock();
                    if (!queue.isEmpty()){
                        queue.remove();
                    }
                    lock.unlock();
                }

            }).start();

        }
    }
}
