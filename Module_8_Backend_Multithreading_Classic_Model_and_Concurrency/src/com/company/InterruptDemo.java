package com.company;

public class InterruptDemo {
    public static void main(String ... args) {
        InterDemo t1 = new InterDemo();
        t1.start();
        try {
            Thread.sleep(10);
            t1.interrupt();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

class InterDemo extends Thread {
    @Override
    public void run() {
        try {
            System.out.println("Task is running");
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            //throw new RuntimeException("Thread was interrupted");
            System.out.println(ex.getMessage());
        }
        if (!Thread.currentThread().interrupted()) {
            System.out.println("Thread continues");
        }
    }
}
