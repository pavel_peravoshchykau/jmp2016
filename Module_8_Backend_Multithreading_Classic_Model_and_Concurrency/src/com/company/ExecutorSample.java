package com.company;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorSample {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Runnable task1 = () -> {
            System.out.println("Hello from task 1 " + Thread.currentThread().getName());
        };

        Runnable task2 = () -> {
            System.out.println("Hello from task 2 " + Thread.currentThread().getName());
        };

        executorService.submit(task1);
        executorService.submit(task2);
        executorService.submit(task2);


        Thread.sleep(1000);
        System.out.println("trying to shutdown");
        executorService.shutdown();

    }
}
