package com.company;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TryLockSample {
    public static void main(String[] args) throws InterruptedException {
        Lock lock = new ReentrantLock();

        new Thread(() -> {
            lock.lock();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
            System.out.println("I'm new Thread, Hello!");
            lock.unlock();
        });

        Thread.sleep(100);

        if (lock.tryLock(2, TimeUnit.SECONDS)) {
            System.out.println("I'm main thread and I catch " + lock);
            lock.unlock();
        }
    }
}
