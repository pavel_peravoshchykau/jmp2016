package com.company;

public class InterruptDemo2 {
    public static void main(String[] args) throws InterruptedException {
        InterThread t1 = new InterThread();
        t1.start();
        Thread.sleep(1000);
        t1.interrupt();
        Thread.sleep(10);
    }
}

class  InterThread extends Thread {
    @Override
    public void run() {
        int i = 0;
        while(!Thread.interrupted()) {
            System.out.println(i++);
        }
        System.out.println("I'm alive");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println(i--);
        }
    }
}