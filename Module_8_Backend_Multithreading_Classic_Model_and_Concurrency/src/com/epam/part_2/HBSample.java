package com.epam.part_2;

/**
 * Created by Alexey_Zinovyev on 18-Aug-16.
 */
public class HBSample {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(()-> System.out.println("I'm alive"));
        System.out.println("Before start");
        t.start();
        Thread.sleep(10);
        System.out.println("I'm from main thread");
        t.join();
        System.out.println("I'm joined in main thread");




    }
}
