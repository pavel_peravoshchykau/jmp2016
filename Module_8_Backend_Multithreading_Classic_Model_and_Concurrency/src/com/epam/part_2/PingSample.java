package com.epam.part_2;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alexey_Zinovyev on 18-Aug-16.
 */
public class PingSample {
    public static void main(String[] args) {
        ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
        service.schedule(()->{
            System.out.println("Ping google.com");
        }, 1, TimeUnit.SECONDS);
    }
}
