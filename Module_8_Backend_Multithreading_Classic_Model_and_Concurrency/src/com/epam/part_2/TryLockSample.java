package com.epam.part_2;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Alexey_Zinovyev on 18-Aug-16.
 */
public class TryLockSample {
    public static void main(String[] args) throws InterruptedException {
        Lock lock = new ReentrantLock();

        new Thread(()->{
            lock.lock();
            try {
                Thread.sleep(3000);
                System.out.println("I'm new Thread, love me!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }).start();

        Thread.sleep(100);
        if(lock.tryLock(2, TimeUnit.SECONDS)){
            System.out.println("I'm Main thread and I catch" +
                    " lock");
            lock.unlock();
        }
    }
}
