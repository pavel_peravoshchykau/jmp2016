package com.epam.part_2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alexey_Zinovyev on 18-Aug-16.
 */
public class ExecutorSample {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newSingleThreadExecutor();


        Runnable task1 = () -> {
            System.out.println("Hello from task 1 " + Thread.currentThread().getName());
        };

        Runnable task2 = () -> {
            System.out.println("Hello from task 2 " + Thread.currentThread().getName());
        };


        executor.submit(task1);
        executor.submit(task2);



        System.out.println("Trying to shutdown");
        executor.shutdown();
        try {
            executor.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            executor.shutdownNow();
            System.out.println("Pool is dead");
        }


    }
}
