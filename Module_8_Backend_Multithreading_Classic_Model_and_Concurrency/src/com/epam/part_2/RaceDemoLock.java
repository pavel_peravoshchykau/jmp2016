package com.epam.part_2;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Alexey_Zinovyev on 16-Aug-16.
 */
public class RaceDemoLock {

    public Integer counter = 0;
    public static Lock lock = new ReentrantLock();
    //public static Lock lock2 = new ReentrantLock();

    public static void main(String[] args) throws InterruptedException {

        RaceDemoLock r = new RaceDemoLock();

        Thread t1 = new Thread(() -> {

            //int j = 0;
            for(int i = 0; i < 1_000_000; i++){
                lock.lock();
                    r.counter++;
                lock.unlock();
               // j++;
               // System.out.println(j);

            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1_000_000; i++) {
                lock.lock();
                    r.counter--;
                lock.unlock();

            }
        });

        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("Result " + r.counter);

    }
}
