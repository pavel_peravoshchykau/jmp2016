package com.epam;

/**
 * Created by Alexey_Zinovyev on 16-Aug-16.
 */
public class InterruptDemo2 {
    public static void main(String[] args) throws InterruptedException {
        InterThread t1 = new InterThread();
        t1.start();
        Thread.sleep(10);
        t1.interrupt();
        Thread.sleep(10);
        t1.interrupt();

    }
}

class InterThread extends Thread{
    public void run(){
        int i = 0;
        while(!Thread.interrupted()){
            System.out.println(i++);
        }
        System.out.println("I'm alive ");
        while(!Thread.currentThread().isInterrupted()){
            System.out.println(i--);
        }
    }
}
