package com.epam;

/**
 * Created by Alexey_Zinovyev on 16-Aug-16.
 */
public class InterruptDemo {
    public static void main(String[] args) {
        InterDemo t1 = new InterDemo();
        t1.start();
        try {
            Thread.sleep(10);
            t1.interrupt();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}


class InterDemo extends Thread {
    public void run() {
        while(!isInterrupted()){
            System.out.println("Thread continues running");
        }

    }
}