package com.epam;

/**
 * Created by Alexey_Zinovyev on 16-Aug-16.
 */
public class RaceDemo {

    public Integer counter = 0;

    public static void main(String[] args) throws InterruptedException {

        RaceDemo r = new RaceDemo();

        Thread t1 = new Thread(() -> {

            int j = 0;
            for(int i = 0; i < 1_000_000; i++){
                synchronized (r){
                    r.counter++;
                }
                j++;
                System.out.println(j);

            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 1_000_000; i++) {
                synchronized (r) {
                    r.counter--;
                }

            }
        });

        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("Result " + r.counter);

    }
}
