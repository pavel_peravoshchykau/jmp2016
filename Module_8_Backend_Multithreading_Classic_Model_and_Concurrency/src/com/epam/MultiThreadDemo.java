package com.epam;

/**
 * Created by Alexey_Zinovyev on 16-Aug-16.
 */
public class MultiThreadDemo {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());
        for (int i = 0; i < 100; i++) {
            new Thread("" + i){
                public void run(){
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Thread " + getName() + " was started!");
                }
            }.start();

        }
    }
}
