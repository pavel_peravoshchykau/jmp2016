package com.epam.mentoring.concurrency;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurrentProducerConsumerRunner extends ProducerConsumerRunner {
    private Lock lock = new ReentrantLock();
    private Queue<Long> randomQueue;

    public ConcurrentProducerConsumerRunner(long maxOps) {
        super(maxOps);
        randomQueue = new LinkedList<>();
    }

    @Override
    public void createProducerAndConsumer() {
        producer = new Thread(() -> {
            while(shouldProceed()) {
                lock.lock();
                randomQueue.add(Math.round(Math.random()*100));
                writeOps++;
                lock.unlock();
            }
        });
        consumer = new Thread(() -> {
            while(shouldProceed()) {
                if (!randomQueue.isEmpty()) {
                    lock.lock();
                    randomSumm += randomQueue.remove();
                    lock.unlock();
                    readOps++;
                } else {
                    try {
                        producer.join(10);
                    } catch (InterruptedException e) {
                        System.out.println("consumer was interrupted while waiting");
                    }
                }
            }
        });
    }

    @Override
    public void interruptJob() {
        super.interruptJob();
        randomQueue.clear();
    }
}
