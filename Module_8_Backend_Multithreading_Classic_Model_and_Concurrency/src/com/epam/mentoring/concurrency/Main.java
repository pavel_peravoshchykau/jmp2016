package com.epam.mentoring.concurrency;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        int maxOps = Integer.MAX_VALUE/10;

        ProducerConsumerRunner syncRunner = new SyncProducerConsumerRunner(maxOps);
        ProducerConsumerRunner syncWaitRunner = new SyncWaitProducerConsumerRunner(maxOps);
        ProducerConsumerRunner concurrentRunner = new ConcurrentProducerConsumerRunner(maxOps);
        ProducerConsumerRunner blockingQueueRunner = new BlockingQueueProducerConsumerRunner(maxOps);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Execute shutdown hook");
            syncRunner.interruptJob();
            syncWaitRunner.interruptJob();
            concurrentRunner.interruptJob();
            blockingQueueRunner.interruptJob();
        }));
        syncRunner.startJob();
        syncWaitRunner.startJob();
        concurrentRunner.startJob();
        blockingQueueRunner.startJob();
    }
}
