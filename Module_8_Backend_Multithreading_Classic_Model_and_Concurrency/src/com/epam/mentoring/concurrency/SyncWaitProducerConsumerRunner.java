package com.epam.mentoring.concurrency;

import java.util.LinkedList;
import java.util.Queue;

public class SyncWaitProducerConsumerRunner extends ProducerConsumerRunner {

    private Queue<Long> randomQueue;

    public SyncWaitProducerConsumerRunner(long maxOps) {
        super(maxOps);
        randomQueue = new LinkedList<>();
    }

    @Override
    public void createProducerAndConsumer() {
        producer = new Thread(() -> {
            while(shouldProceed()) {
                synchronized (randomQueue) {
                    randomQueue.add(Math.round(Math.random()*100));
                    randomQueue.notify();
                }
                writeOps++;
            }
        });
        consumer = new Thread(() -> {
            while(shouldProceed()) {
                if (!randomQueue.isEmpty()) {
                    synchronized (randomQueue) {
                        randomSumm += randomQueue.remove();
                        randomQueue.notify();
                    }
                    readOps++;
                } else {
                    synchronized (randomQueue) {
                        try {
                            randomQueue.wait();
                        } catch (InterruptedException e) {
                            System.out.println("consumer was interrupted while waiting");
                        }
                    }
                }
            }
        });
    }

    @Override
    public void interruptJob() {
        super.interruptJob();
        randomQueue.clear();
    }
}
