package com.epam.mentoring.concurrency;

import java.util.Calendar;

public abstract class ProducerConsumerRunner {

    long randomSumm;
    long readOps;
    long writeOps;
    private long maxOps;
    private long startTime;
    private boolean isStarted;
    Thread producer;
    Thread consumer;

    public ProducerConsumerRunner(long maxOps) {
        this.maxOps = maxOps;
    }

    public abstract void createProducerAndConsumer() ;

    public void startJob() throws InterruptedException {
        System.out.println("------- Start " + this.getClass().getSimpleName()+" ------------");
        createProducerAndConsumer();
        producer.start();
        consumer.start();
        startTime = Calendar.getInstance().getTimeInMillis();
        isStarted = true;
        producer.join();
        consumer.join();
    }

    public void printStats() {
        if (!isStarted) {
            System.out.println("------- " + this.getClass().getSimpleName() + " was not started ------------");
            return;
        }
        long seconds;
        System.out.println("-------" + this.getClass().getSimpleName() + " run result------------");
        System.out.println("write operations: " + writeOps);
        System.out.println("read operations: " + readOps);
        seconds = (Calendar.getInstance().getTimeInMillis() - startTime)/1000;
        System.out.println("Total operations: "  +(writeOps + readOps) + " in "
                + seconds + " seconds");
        if (seconds > 0) {
            System.out.println("Ops / sec = " + ((writeOps + readOps) / seconds));
        }
        System.out.println("----------------------------------");
        System.out.println("producer is alive : " + producer.isAlive());
        System.out.println("consumer is alive : " + consumer.isAlive());
    }

    public void interruptJob() {
        if (producer != null && consumer != null ) {
            producer.interrupt();
            consumer.interrupt();
        }
        printStats();
    }

    boolean shouldProceed() {
        return !Thread.currentThread().isInterrupted() && ((readOps + writeOps) < maxOps);
    }
}
