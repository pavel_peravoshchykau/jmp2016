package com.epam.mentoring.concurrency;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingQueueProducerConsumerRunner extends ProducerConsumerRunner {
    private BlockingQueue<Long> randomQueue;

    public BlockingQueueProducerConsumerRunner(long maxOps) {
        super(maxOps);
        randomQueue = new LinkedBlockingQueue<>();
    }

    @Override
    public void createProducerAndConsumer() {
        producer = new Thread(() -> {
            while(shouldProceed()) {
                try {
                    randomQueue.put(Math.round(Math.random()*100));
                } catch (InterruptedException e) {
                    System.err.println("producer interrupted while waiting to put");
                }
                writeOps++;
            }
        });
        consumer = new Thread(() -> {
            while(shouldProceed()) {
                try {
                    randomSumm += randomQueue.take();
                } catch (InterruptedException e) {
                    System.err.println("producer interrupted while waiting to take");
                }
                readOps++;
            }
        });
    }

    @Override
    public void interruptJob() {
        super.interruptJob();
        randomQueue.clear();
    }
}
