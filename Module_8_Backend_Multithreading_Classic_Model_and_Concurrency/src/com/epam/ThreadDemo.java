package com.epam;

/**
 * Created by Alexey_Zinovyev on 16-Aug-16.
 */
public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        /*MyThread t = new MyThread();
        t.start();

        Thread thread = new Thread(new MyRunnableClass());
        thread.start();



        Thread thread2 = new Thread(myRunnable);
        thread2.start();*/

        Runnable myRunnable = () -> System.out.println("Runnable is running");

        Thread threadNamed = new Thread(myRunnable, "New Thread");
        threadNamed.start();
        threadNamed.start();

        Thread.sleep(100);
        Thread currentThread = Thread.currentThread();
        System.out.println(currentThread.getName() + " " + currentThread.getPriority() + " "
        + currentThread.getState());

    }
}
class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println("MyThread is running");

    }
}


class MyRunnableClass implements Runnable {

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("MyRunnable");
    }
}