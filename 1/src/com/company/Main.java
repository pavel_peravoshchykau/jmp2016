package com.company;

import com.company.function.beans.FunctionResult;
import com.company.function.constants.Functions;
import com.company.function.utils.FunctionQueryExecutor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static List<FunctionResult> processFunctionFromInputStream(InputStream stream, boolean printResult) {
        List<FunctionResult> results = new ArrayList<>();
        FunctionQueryExecutor functionQueryExecutor = new FunctionQueryExecutor();
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            String input;
            FunctionResult result;
            while((input = br.readLine()) != null){
                if (input.isEmpty()) {
                    System.out.println("input is empty");
                    System.out.println("Available functions are: ");
                    for (Functions function : Functions.values()) {
                        System.out.println(function.name());
                    }
                } else if (input.equalsIgnoreCase("exit")) {
                    System.exit(0);
                } else {
                    try {
                        result = new FunctionResult(input, functionQueryExecutor.executeFunctionQuery(input).toString());
                    } catch (InstantiationException e) {
                        System.err.println("Wrong request: " + e);
                        continue;
                    }
                    results.add(result);
                    if (printResult) {
                        System.out.println(result);
                    }
                }
            }
        } catch(IOException io){
            System.err.println("Unexpected IO Exception: " + io);
            io.printStackTrace();
        }
        return results;
    }

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Please enter function name and array of numbers in format:  <function> number1 number2 ... numberN (e.g. sum 12 2 81)\n");
            processFunctionFromInputStream(System.in, true);
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(args[0]);
                if (args.length > 1) {
                    FileWriter fileWriter = new FileWriter("output.txt");
                    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                    for (FunctionResult arg : processFunctionFromInputStream(fileInputStream, false)) {
                        bufferedWriter.write(arg.toString());
                        bufferedWriter.newLine();
                    }
                    bufferedWriter.close();
                } else {
                    processFunctionFromInputStream(fileInputStream, false).forEach(System.out::println);
                }
            } catch (FileNotFoundException e) {
                System.err.println(MessageFormat.format("File with name {0} was not found", args[0]));
            } catch (IOException e) {
                System.err.println(MessageFormat.format("Output file with name {0} not found. Exception: {1}", args[1], e));
            }
        }
    }
}
