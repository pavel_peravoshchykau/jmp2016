package com.company.function.factories;
import com.company.function.constants.Functions;
import com.company.function.IFunction;
import java.text.MessageFormat;
import java.util.function.Function;

public class FunctionFactory {

    /**
     * Creates a new instance of the IFunction derived class represented by the specified String parameter.
     *
     * @param   functionType the name of the function.
     * @return  a newly allocated instance of the IFunction derived class represented by function type.
     * @exception InstantiationException if the instance of the IFunction derived class wasn't created.
     */
    public static IFunction getFunction(String functionType) throws InstantiationException {
        IFunction function = null;
        try {
            Class functionClass = Functions.valueOf(functionType.toUpperCase()).getFunctionClass();
            try {
                function = (IFunction) functionClass.newInstance();
            } catch (IllegalAccessException | InstantiationException e) {
                System.err.println(MessageFormat.format("Function class \"{0}\" is not available or its default " +
                        "constructor is not accessible.", functionClass));
            }
        } catch (IllegalArgumentException ex) {
            System.err.println("No such function: " + functionType);
        }
        if (function == null) {
            throw new InstantiationException("Error during function creation");
        }
        return function;
    }
}
