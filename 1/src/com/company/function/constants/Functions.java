package com.company.function.constants;

import com.company.function.implementations.FunctionAvg;
import com.company.function.implementations.FunctionMax;
import com.company.function.implementations.FunctionMin;
import com.company.function.implementations.FunctionSum;

public enum Functions {
    SUM (FunctionSum.class),
    MAX (FunctionMax.class),
    MIN (FunctionMin.class),
    AVG (FunctionAvg.class);

    private final Class functionClass;

    Functions(Class functionClass) {
        this.functionClass = functionClass;
    }

    public Class getFunctionClass() {
        return functionClass;
    }
}
