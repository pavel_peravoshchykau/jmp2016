package com.company.function.implementations;

import com.company.function.IFunction;

public class FunctionAvg implements IFunction {

    @Override
    public double execute(double[] args) {
        double result = 0d;
        for (double arg : args) {
            result += arg;
        }
        if (result == 0) {
            return result;
        }
        return result/args.length;
    }
}
