package com.company.function.implementations;

import com.company.function.IFunction;

public class FunctionSum implements IFunction {

    @Override
    public double execute(double[] args) {
        double result = 0d;
        for (double arg : args) {
            result += arg;
        }
        return result;
    }
}
