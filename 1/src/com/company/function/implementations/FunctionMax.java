package com.company.function.implementations;

import com.company.function.IFunction;

public class FunctionMax implements IFunction {

    @Override
    public double execute(double[] args) {
        double result = 0d;
        for (double arg : args) {
            if (arg > result) {
                result = arg;
            }
        }
        return result;
    }
}
