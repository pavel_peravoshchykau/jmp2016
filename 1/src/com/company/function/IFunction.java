package com.company.function;

public interface IFunction {

    double execute(double[] args);
}
