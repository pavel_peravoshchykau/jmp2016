package com.company.function.utils;

import com.company.function.IFunction;
import com.company.function.factories.FunctionFactory;

import java.util.ArrayList;
import java.util.List;

public class FunctionQueryExecutor {

    public Double executeFunctionQuery(String query) throws InstantiationException {
        String[] args = query.split(" ");
        String functionId = args[0].trim();
        IFunction function = FunctionFactory.getFunction(functionId);
        List<Double> numbers = new ArrayList<>();
        for (int i = 1; i < args.length; i++) {
            if (args[i].isEmpty()){
                continue;
            }
            numbers.add(Double.parseDouble(args[i].trim()));
        }
        double[] arguments = new double[numbers.size()];
        for (int i = 0; i < numbers.size(); i++) {
            arguments[i] = numbers.get(i).doubleValue();
        }
        return function.execute(arguments);
    }
}
