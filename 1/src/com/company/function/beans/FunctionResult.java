package com.company.function.beans;

import java.text.MessageFormat;

public class FunctionResult {

    private final String query;
    private final String result;

    public FunctionResult(String query, String result) {
        this.query = query;
        this.result = result;
    }

    public String getQuery() {
        return query;
    }

    public String getResult() {
        return result;
    }

    public String toString() {
        return MessageFormat.format("Function( {0} ) return: {1}", query, result);
    }
}
