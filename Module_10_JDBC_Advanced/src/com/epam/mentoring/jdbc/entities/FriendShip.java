package com.epam.mentoring.jdbc.entities;

import java.sql.Date;

public class FriendShip {
    private int userId1;
    private int userId2;
    private Date timestamp;

    public FriendShip(int userId1, int userId2, Date timestamp) {
        this.userId1 = userId1;
        this.userId2 = userId2;
        this.timestamp = timestamp;
    }

    public int getUserId1() {
        return userId1;
    }

    public int getUserId2() {
        return userId2;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FriendShip that = (FriendShip) o;

        if (userId1 != that.userId1) return false;
        return userId2 == that.userId2;

    }

    @Override
    public int hashCode() {
        int result = userId1;
        result = 31 * result + userId2;
        return result;
    }
}
