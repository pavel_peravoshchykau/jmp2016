package com.epam.mentoring.jdbc.entities;

import java.sql.Date;

public class Like {
    private int postId;
    private int userId;
    private Date timestamp;

    public Like(int postId, int userId, Date timestamp) {
        this.postId = postId;
        this.userId = userId;
        this.timestamp = timestamp;
    }

    public int getPostId() {
        return postId;
    }

    public int getUserId() {
        return userId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Like like = (Like) o;

        if (postId != like.postId) return false;
        return userId == like.userId;

    }

    @Override
    public int hashCode() {
        int result = postId;
        result = 31 * result + userId;
        return result;
    }
}
