package com.epam.mentoring.jdbc;

import com.epam.mentoring.jdbc.datasource.DataSourceManager;
import com.mchange.v2.c3p0.impl.AbstractPoolBackedDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.beans.PropertyVetoException;

@org.springframework.context.annotation.Configuration
@ComponentScan
public class Configuration {

    @Bean
    AbstractPoolBackedDataSource getAbstractPoolBackedDataSource() throws PropertyVetoException {
        return DataSourceManager.getDataSource();
    }
}
