package com.epam.mentoring.jdbc.datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.stereotype.Component;

import java.beans.PropertyVetoException;

@Component
public class DataSourceManager {

    //driver class org.h2.Driver
    //jdbc url jdbc:h2:~/test
    //USERNAME: 'sa' pass: empty

    public static ComboPooledDataSource getDataSource() throws PropertyVetoException {

        ComboPooledDataSource cpds = new ComboPooledDataSource();
        cpds.setDriverClass("org.h2.Driver");
        //jdbc:h2:tcp://localhost/server~/dbname
        //jdbc:h2:target/h2/ps;AUTO_SERVER=TRUE
        //jdbc:h2:tcp://localhost/~/test
        cpds.setJdbcUrl("jdbc:h2:tcp://localhost/~/test");
        cpds.setUser("sa");
        cpds.setPassword("");
        return cpds;
    }
}
