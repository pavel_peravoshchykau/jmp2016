package com.epam.mentoring.jdbc.dao;

import com.epam.mentoring.jdbc.entities.Like;
import com.epam.mentoring.jdbc.entities.User;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class LikesDAO extends TaskDAO {

    private final String insertLike = "INSERT INTO LIKES VALUES (?,?,?)";
    private final String getPostLikes = "SELECT * FROM LIKES WHERE POSTID = ?";
    private final String getUserLikes = "SELECT * FROM LIKES WHERE USERID = ?";

    public List<Like> getUserLikes(int userId) {
        List<Like> likes = new ArrayList<>();
        try (Connection cn = getConnection();
             PreparedStatement ps = cn.prepareStatement(getUserLikes)){
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                likes.add(new Like(rs.getInt(1), rs.getInt(2), rs.getDate(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return likes;
    }

    public void insertLike(Like like) {
        try (Connection cn = getConnection();
        PreparedStatement ps = cn.prepareStatement(insertLike)) {
            prepareInsertLikeStatement(ps, like).execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int[] insertLikesWithBatch(Set<Like> likes) {
        int[] response = new int[0];
        try (Connection cn = getConnection();
             PreparedStatement ps = cn.prepareStatement(insertLike)){
            cn.setAutoCommit(false);
            for (Like like : likes) {
                prepareInsertLikeStatement(ps, like);
                ps.addBatch();
            }
            response = ps.executeBatch();
            cn.commit();
            cn.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return response;
    }

    private PreparedStatement prepareInsertLikeStatement(PreparedStatement ps, Like like) throws SQLException {
        ps.setInt(1, like.getPostId());
        ps.setInt(2, like.getUserId());
        ps.setDate(3, like.getTimestamp());
        return ps;
    }

    public void truncateLikes() {
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()){
            statement.execute("TRUNCATE TABLE LIKES");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
