package com.epam.mentoring.jdbc.dao;

import com.epam.mentoring.jdbc.entities.Like;
import com.epam.mentoring.jdbc.entities.Post;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service
public class PostsDAO extends TaskDAO {
    //Posts(id, userId, text, timestamp),

    private final String insertPost = "INSERT INTO POSTS VALUES (?,?,?,?)";
    private final String getUserPosts = "SELECT * FROM POSTS WHERE USERID = ?";

    public List<Post> getUserPosts(int userId) {
        List<Post> posts = new ArrayList<>();
        try (Connection cn = getConnection();
             PreparedStatement ps = cn.prepareStatement(getUserPosts)){
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                posts.add(new Post(rs.getInt(1), rs.getInt(2), rs.getClob(3).toString(), rs.getDate(4)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posts;
    }

    public void insertPost(Post post) {
        try (Connection cn = getConnection();
             PreparedStatement ps = cn.prepareStatement(insertPost)) {
            prepareInsertLikeStatement(ps, post).execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private PreparedStatement prepareInsertLikeStatement(PreparedStatement ps, Post post) throws SQLException {
        ps.setInt(1, post.getId());
        ps.setInt(2, post.getUserId());
        ps.setString(3, post.getText());
        ps.setDate(4, post.getTimestamp());
        return ps;
    }

    public void truncatePosts() {
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()){
            statement.execute("TRUNCATE TABLE POSTS");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
