package com.epam.mentoring.jdbc.dao;

import com.epam.mentoring.jdbc.entities.FriendShip;
import com.epam.mentoring.jdbc.entities.Like;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service
public class FriendshipsDAO extends TaskDAO {

    private final String insertFriendship = "INSERT INTO FRIENDSHIPS VALUES (?,?,?)";
    private final String getUserFriendships = "SELECT * FROM FRIENDSHIPS WHERE USERID1 = ? OR USERID1 = ?";

    public List<FriendShip> getFriengshipsForUser(int userId) {
        List<FriendShip> friendShips = new ArrayList<>();
        try (Connection cn = getConnection();
             PreparedStatement ps = cn.prepareStatement(getUserFriendships)){
            ps.setInt(1, userId);
            ps.setInt(2, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                friendShips.add(new FriendShip(rs.getInt(1), rs.getInt(2), rs.getDate(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendShips;
    }

    public void insertFriendship(FriendShip friendship) {
        try (Connection cn = getConnection();
             PreparedStatement ps = cn.prepareStatement(insertFriendship)) {
            prepareInsertLikeStatement(ps, friendship).execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private PreparedStatement prepareInsertLikeStatement(PreparedStatement ps, FriendShip friendship) throws SQLException {
        ps.setInt(1, friendship.getUserId1());
        ps.setInt(2, friendship.getUserId2());
        ps.setDate(3, friendship.getTimestamp());
        return ps;
    }

    public void truncateFriendships() {
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()){
            statement.execute("TRUNCATE TABLE FRIENDSHIPS");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
