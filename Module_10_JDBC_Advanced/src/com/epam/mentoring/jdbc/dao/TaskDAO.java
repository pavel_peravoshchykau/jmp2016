package com.epam.mentoring.jdbc.dao;

import com.mchange.v2.c3p0.impl.AbstractPoolBackedDataSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;
import java.sql.SQLException;

abstract class TaskDAO {

    @Autowired
    private AbstractPoolBackedDataSource dataSource;

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

}
