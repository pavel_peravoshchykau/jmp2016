package com.epam.mentoring.jdbc.dao;

import com.epam.mentoring.jdbc.entities.User;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service
public class UsersDAO extends TaskDAO {

    private final String insertUser = "INSERT INTO USERS VALUES (?,?,?,?)";
    private final String getUser = "SELECT * FROM USERS WHERE ID = ?";
    private final String getUsers = "SELECT * FROM USERS";

    public User getUser(int id) {
        User user = null;
        try (Connection cn = getConnection();
             PreparedStatement ps = cn.prepareStatement(getUser)){
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                user = buildUser(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        try (Connection cn = getConnection();
             PreparedStatement ps = cn.prepareStatement(getUsers)){
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                users.add(buildUser(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }


    public void insertUser(User user) {
        try (Connection cn = getConnection();
             PreparedStatement ps = cn.prepareStatement(insertUser)){

            prepareInsertStatement(ps, user).execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void insertAbstractUsers(int numberOfUsers) {
        for (int i = 0; i < numberOfUsers; i++) {
            insertUser(new User(i));
        }
    }

    public int[] insertAbstractUsersWithBatch(int numberOfUsers) {
        int[] response = new int[0];
        try (Connection cn = getConnection();
             PreparedStatement ps = cn.prepareStatement(insertUser)){
            cn.setAutoCommit(false);
            for (int i = 0; i < numberOfUsers; i++) {
                prepareInsertStatement(ps, new User(i));
                ps.addBatch();
            }
            response = ps.executeBatch();
            cn.commit();
            cn.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return response;
    }

    public void truncateUsers() {
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement()){
            statement.execute("TRUNCATE TABLE USERS");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private PreparedStatement prepareInsertStatement(PreparedStatement ps, User user) throws SQLException {
        ps.setInt(1, user.getId());
        ps.setString(2, user.getName());
        ps.setString(3, user.getSurname());
        ps.setDate(4, user.getBirthDate());
        return ps;
    }

    private User buildUser(ResultSet rs) throws SQLException {
        return new User(rs.getInt("ID"), rs.getString("NAME"), rs.getString("SURNAME"), rs.getDate("BIRTHDATE"));
    }
}
