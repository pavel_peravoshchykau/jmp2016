package com.epam.mentoring.jdbc.services;

import com.epam.mentoring.jdbc.dao.FriendshipsDAO;
import com.epam.mentoring.jdbc.dao.LikesDAO;
import com.epam.mentoring.jdbc.dao.PostsDAO;
import com.epam.mentoring.jdbc.dao.UsersDAO;
import com.epam.mentoring.jdbc.entities.FriendShip;
import com.epam.mentoring.jdbc.entities.Like;
import com.epam.mentoring.jdbc.entities.Post;
import com.epam.mentoring.jdbc.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

@Component
public class MockFriendshipEngine {

    @Autowired
    UsersDAO usersDAO;
    @Autowired
    PostsDAO postsDAO;
    @Autowired
    LikesDAO likesDAO;
    @Autowired
    FriendshipsDAO friendshipsDAO;

    public List<User> getUsers() {
        return usersDAO.getUsers();
    }

    public List<FriendShip> getFriendShips(int userId){
        return friendshipsDAO.getFriengshipsForUser(userId);
    }

    public List<Post> getPosts(int userId){
        return postsDAO.getUserPosts(userId);
    }

    public List<Like> getLikes(int userId) {
        return likesDAO.getUserLikes(userId);
    }

    public void buildData(int friendshipsNumber, int likesNumber, int usersNumber) throws SQLException {
        usersDAO.truncateUsers();
        usersDAO.insertAbstractUsersWithBatch(usersNumber);
        friendshipsDAO.truncateFriendships();
        Set<FriendShip> friendShips = createFriendships(friendshipsNumber);
        for (FriendShip friendShip : friendShips) {
            friendshipsDAO.insertFriendship(friendShip);
        }
        postsDAO.truncatePosts();
        Set<Post> posts = generatePosts();
        for (Post post : generatePosts()) {
            postsDAO.insertPost(post);
        }
        likesDAO.truncateLikes();
        Set<Like> likes = generateLikes(likesNumber);
        likesDAO.insertLikesWithBatch(likes);
    }

    private Set<FriendShip> createFriendships(int friendshipsNumber) {
        List<User> users = usersDAO.getUsers();
        User tempUser = null;
        Set<FriendShip> friendShips = new HashSet<>();

        while (friendShips.size() < friendshipsNumber){
            Collections.shuffle(users);
            for (User user : users) {
                if (tempUser == null) {
                    tempUser = user;
                } else {
                    friendShips.add(new FriendShip(tempUser.getId(), user.getId(), new Date(Calendar.getInstance().getTimeInMillis())));
                    tempUser = user;
                }
            }
        }
        return friendShips;
    }

    private Set<Post> generatePosts(){
        List<User> users = usersDAO.getUsers();
        Set<Post> posts = new HashSet<>();
        for (User user : users) {
            posts.add(new Post((int)Math.round(Math.random()*10000000), user.getId(), "cool post from " + user.getName(), new Date(Calendar.getInstance().getTimeInMillis())));
        }
        return posts;
    }

    private Set<Like> generateLikes(int likesNumber) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2015, 3, 15);
        Set<Like> likes = new HashSet<>();
        List<User> users = usersDAO.getUsers();
        while (likes.size() < likesNumber) {
            Collections.shuffle(users);
            for (User user : users) {
                List<Post> posts = postsDAO.getUserPosts(user.getId());
                if (!posts.isEmpty()) {
                    for (Post post : posts) {
                        for (User user1 : users) {
                            if (user.getId() != user1.getId()) {
                                if (Math.random() > 0.5) {
                                    likes.add(new Like(post.getId(), user1.getId(), new Date(Calendar.getInstance().getTimeInMillis())));
                                } else {
                                    likes.add(new Like(post.getId(), user1.getId(), new Date(calendar.getTimeInMillis())));
                                }
                            }
                        }
                    }
                }
            }
        }
        return likes;
    }
}
