package com.epam.mentoring.jdbc;

import com.epam.mentoring.jdbc.entities.Like;
import com.epam.mentoring.jdbc.entities.User;
import com.epam.mentoring.jdbc.services.MockFriendshipEngine;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.beans.PropertyVetoException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Component
public class Main {
/*
CREATE TABLE USERS (ID INT PRIMARY KEY, NAME VARCHAR(255), SURNAME VARCHAR(255), BIRTHDATE DATE);
CREATE TABLE FRIENDSHIPS (USERID1 INT, USERID2 INT, TIMESTAMP DATE, PRIMARY KEY (USERID1, USERID2));
CREATE TABLE POSTS (ID INT PRIMARY KEY, USERID INT, TEXT CLOB, TIMESTAMP DATE);
CREATE TABLE LIKES (POSTID INT, USERID INT, TIMESTAMP DATE, PRIMARY KEY (POSTID, USERID, TIMESTAMP));
 */


    public static void main(String[] args) throws SQLException, PropertyVetoException {
        int usersNumber = 1001;
        int friendshipsNumber = 70001;
        int likesNumber = 300001;
        Calendar calendarBefore = Calendar.getInstance();
        calendarBefore.set(2015, 2, 28);
        Calendar calendarAfter = Calendar.getInstance();
        calendarAfter.set(2015, 4, 1);
        ApplicationContext context = new AnnotationConfigApplicationContext(Configuration.class);

        MockFriendshipEngine friendshipEngine = context.getBean(MockFriendshipEngine.class);

        friendshipEngine.buildData(friendshipsNumber, likesNumber, usersNumber);
        List<User> sociableUsers = new ArrayList<>();
        for (User user : friendshipEngine.getUsers()) {
            if (friendshipEngine.getFriendShips(user.getId()).size() > 100){
                sociableUsers.add(user);
            }
        }
        List<User> popularUsers = new ArrayList<>();

        List<User> users = friendshipEngine.getUsers();
        for (User user : users) {
            List<Like> likes = friendshipEngine.getLikes(user.getId());
            List<Like> likesInMarch = new ArrayList<>();
            for (Like like : likes) {
                if (like.getTimestamp().after(new Date(calendarBefore.getTimeInMillis()))
                        && like.getTimestamp().before(new Date(calendarAfter.getTimeInMillis()))) {
                    likesInMarch.add(like);
                }
            }
            if (likesInMarch.size() > 100) {
                popularUsers.add(user);
            }
        }

        List<User> targetUsers = new ArrayList<>();

        for (User popularUser : popularUsers) {
            for (User sociableUser : sociableUsers) {
                if (popularUser.equals(sociableUser))
                    targetUsers.add(sociableUser);
            }
        }

        System.out.println("Target users size: " + targetUsers.size());
    }
}
